#!/bin/bash

set -e

wasm-pack build e173 --features wasm
cargo run -p postprocess -- e173/pkg
cp README.md e173/pkg
