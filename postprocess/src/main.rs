use std::io::Write;
use std::{env, path::Path};

use oxc::allocator::Allocator;
use oxc::ast::ast::{
    Declaration, ExportNamedDeclaration, Expression, ModuleDeclaration, Statement, StringLiteral,
    TSEnumMemberName,
};
use oxc::parser::Parser;
use oxc::span::SourceType;

const TYPES_FILE_NAME: &str = "e173.d.ts";
const JS_OUT_FILE_NAME: &str = "e173_bg.js";

fn main() -> Result<(), String> {
    let pkg_dir = env::args()
        .nth(1)
        .ok_or_else(|| "Must provide the path to the pkg directory".to_owned())?;
    let pkg_dir_path = Path::new(&pkg_dir);
    let mut types_path = pkg_dir_path.to_path_buf();
    types_path.push(TYPES_FILE_NAME);

    let mut out_path = pkg_dir_path.to_path_buf();
    out_path.push(JS_OUT_FILE_NAME);
    let mut out_file = std::fs::OpenOptions::new()
        .append(true)
        .open(&out_path)
        .map_err(|_| format!("Couldn't open {} for writing", out_path.display()))?;

    let source_text = std::fs::read_to_string(&types_path)
        .map_err(|_| format!("Couldn't open {} for reading", types_path.display()))?;
    let allocator = Allocator::default();
    let source_type = SourceType::from_path(&types_path).unwrap();
    let ret = Parser::new(&allocator, &source_text, source_type).parse();

    if !ret.program.source_type.is_typescript_definition() {
        return Err("File provided was not a typescript definitions file".into());
    }

    for statement in &ret.program.body {
        let Statement::ModuleDeclaration(module_decl) = statement else {
            continue;
        };

        let ModuleDeclaration::ExportNamedDeclaration(ref export_named_decl) = **module_decl else {
            continue;
        };

        let ExportNamedDeclaration {
            declaration: Some(Declaration::TSEnumDeclaration(ref ts_enum)),
            ..
        } = **export_named_decl
        else {
            continue;
        };

        let identifier = ts_enum.id.name.as_str();

        let mut js_export = format!("export const {} = Object.freeze({{", identifier);

        let mut valid = true;

        for member in &ts_enum.members {
            let Some(Expression::StringLiteral(ref string_lit)) = member.initializer else {
                // Not a string enum
                valid = false;
                break;
            };

            let StringLiteral { ref value, .. } = **string_lit;

            if let TSEnumMemberName::Identifier(identifier) = &member.id {
                js_export.push_str(&format!("{}:\"{}\",", identifier.name, value));
                continue;
            }
            if let TSEnumMemberName::StringLiteral(literal) = &member.id {
                js_export.push_str(&format!("\"{}\":\"{}\",", literal.value, value));
                continue;
            }
            // Invalid identifier
            valid = false;
        }

        if valid {
            js_export.push_str("});\n");
            write!(out_file, "{}", js_export)
                .map_err(|_| "Error writing to output file".to_owned())?;
        }
    }

    if ret.errors.is_empty() {
        println!("Postprocessed Successfully.");
    } else {
        for error in ret.errors {
            let error = error.with_source_code(source_text.clone());
            println!("{error:?}");
            println!("Parsed with Errors.");
        }
    }

    Ok(())
}
