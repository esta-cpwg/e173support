# WASM Postprocessing Crate

This entire crate exists to work around [this issue](https://github.com/rustwasm/wasm-bindgen/issues/3057). It may be able to be deleted when that issue is fixed.
