use e173::tsify::Tsify;
use std::borrow::Cow;

#[derive(Tsify)]
#[tsify(into_wasm_abi, from_wasm_abi)]
struct Borrow<'a> {
    raw: &'a str,
    cow: Cow<'a, str>,
}
