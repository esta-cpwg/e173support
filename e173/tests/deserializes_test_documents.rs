use std::{fs::File, path::PathBuf};

use e173::schema::{validate_doc, E173Document};
use itertools::Itertools;
use jsonschema::Validator;
// use reqwest::blocking::get;
use rstest::{fixture, rstest};

// const SCHEMA_REF: &str = "a801909222a07e9e956ae4406b8bba7adf3e0d4c";

#[fixture]
#[once]
fn udr_json_schema() -> Validator {
    //     let schema_text = get(
    //             format!("https://gitlab.com/esta-cpwg/e173/-/raw/{SCHEMA_REF}/schemas/draft-2024-1/full/udr-document.json")
    //         )
    //         .unwrap()
    //         .json()
    //         .unwrap();
    //     Validator::options()
    //         .with_draft(Draft::Draft201909)
    //         .build(&schema_text)
    //         .unwrap()
    todo!()
}

#[rstest]
#[ignore]
fn test_docs_pass_both_schema_and_validation(
    #[files("tests/documents/pass_both/*.json")] path: PathBuf,
    udr_json_schema: &Validator,
) {
    let file = File::open(path).unwrap();
    let json_val: serde_json::Value = serde_json::from_reader(&file).unwrap();

    {
        let validation_result = udr_json_schema.validate(&json_val);
        let result_is_ok = validation_result.is_ok();
        let errors = validation_result
            .err()
            .unwrap_or(Box::new(std::iter::empty()))
            .map(|err| {
                format!(
                    "kind: {:?}, instance_path: {}, schema_path: {}",
                    err.kind, err.instance_path, err.schema_path
                )
            })
            .join(",");
        assert!(result_is_ok, "[{}]", errors);
    }

    let doc: E173Document = serde_json::from_value(json_val).unwrap();
    validate_doc(&doc).unwrap();
}

#[rstest]
#[ignore]
fn test_docs_pass_schema_but_fail_validation(
    #[files("tests/documents/pass_schema_fail_validation/*.json")] path: PathBuf,
    udr_json_schema: &Validator,
) {
    let file = File::open(path).unwrap();
    let json_val: serde_json::Value = serde_json::from_reader(&file).unwrap();

    {
        let validation_result = udr_json_schema.validate(&json_val);
        let result_is_ok = validation_result.is_ok();
        let errors = validation_result
            .err()
            .unwrap_or(Box::new(std::iter::empty()))
            .map(|err| {
                format!(
                    "kind: {:?}, instance_path: {}, schema_path: {}",
                    err.kind, err.instance_path, err.schema_path
                )
            })
            .join(",");
        assert!(result_is_ok, "[{}]", errors);
    }

    let doc: E173Document = serde_json::from_value(json_val).unwrap();
    assert!(validate_doc(&doc).is_err());
}

#[rstest]
#[ignore]
fn test_docs_fail_both_schema_and_validation(
    #[files("tests/documents/fail_both/*.json")] path: PathBuf,
    udr_json_schema: &Validator,
) {
    let file = File::open(path).unwrap();
    let json_val: serde_json::Value = serde_json::from_reader(&file).unwrap();

    assert!(!udr_json_schema.validate(&json_val).is_ok());

    let doc: E173Document = serde_json::from_value(json_val).unwrap();
    assert!(validate_doc(&doc).is_err());
}
