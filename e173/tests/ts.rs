#[cfg(feature = "wasm")]
mod ts {
    mod r#enum;
    mod expandtest;
    mod flatten;
    mod generics;
    mod optional;
    mod rename;
    mod skip;
    mod r#struct;
    mod transparent;
    mod type_override;
    mod wasm;
}
