pub mod controller;
pub mod error;
pub mod schema;
#[cfg(feature = "wasm")]
pub mod tsify;
#[cfg(all(feature = "wasm", target_arch = "wasm32"))]
pub mod wasm;

pub use error::*;

extern crate self as e173;

macro_rules! e173_types {
    ($($decl:item)+) => {$(
        #[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
        #[cfg_attr(
            target_arch = "wasm32",
            derive(crate::tsify::Tsify),
            tsify(from_wasm_abi, into_wasm_abi, maps_as_objects)
        )]
        $decl
    )+};
}

macro_rules! e173_types_no_serde{
    ($($decl:item)+) => {$(
        #[derive(Clone, Debug)]
        #[cfg_attr(
            target_arch = "wasm32",
            derive(crate::tsify::Tsify),
            tsify(from_wasm_abi, into_wasm_abi, maps_as_objects)
        )]
        $decl
    )+};
}

use e173_types;
use e173_types_no_serde;
