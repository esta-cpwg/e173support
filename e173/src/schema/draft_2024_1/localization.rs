use std::collections::HashMap;

use crate::e173_types;

e173_types! {

pub struct DefinitionLocalization {
    #[serde(default)]
    strings: HashMap<String, String>,
    #[serde(default)]
    categories: HashMap<String, String>,
}

pub struct OverlayLocalization {
    #[serde(default)]
    strings: HashMap<String, HashMap<String, String>>,
    #[serde(default)]
    categories: HashMap<String, String>,
}

}
