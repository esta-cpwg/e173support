use std::collections::HashSet;

use crate::e173_types;

use super::misc::{Access, Lifetime};

e173_types! {

pub struct Serializer {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub library: Option<String>,
    pub class: String,
    pub access: HashSet<Access>,
    pub lifetime: Lifetime,
    #[cfg_attr(target_arch = "wasm32", tsify(type = "Record<string, any>"))]
    pub default: Option<serde_json::Value>,
}

pub struct SerializerClass {
    #[serde(rename = "@name")]
    pub name: String,
    #[serde(rename = "@description", skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
}

}
