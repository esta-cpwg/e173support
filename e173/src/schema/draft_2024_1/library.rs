use std::collections::HashMap;

use crate::e173_types;

use super::{
    localization::DefinitionLocalization, parameter::ParameterClass, serializer::SerializerClass,
    structure::StructureClass,
};

e173_types! {

#[serde(rename_all = "camelCase")]
pub struct Library {
    #[serde(rename = "@description")]
    pub description: String,
    pub publish_date: String,
    pub author: String,
    pub parameter_classes: HashMap<String, ParameterClass>,
    pub structure_classes: HashMap<String, StructureClass>,
    pub serializer_classes: HashMap<String, SerializerClass>,
    pub localizations: HashMap<String, DefinitionLocalization>,
}

}
