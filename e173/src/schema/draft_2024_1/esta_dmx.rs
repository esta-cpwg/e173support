use std::collections::HashMap;

use crate::e173_types;

e173_types! {

pub struct EstaDmx {
    pub chunks: HashMap<String, Chunk>,
}

#[serde(rename_all = "camelCase")]
pub struct Chunk {
    // Valid range 0-511, valid size 1-512
    pub offsets: Vec<u16>,
    pub mapping_groups: Vec<MappingGroup>,
}

pub struct MappingGroup {
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub conditions: Vec<Condition>,
    pub mappings: Vec<Mapping>,
}

#[serde(rename_all = "camelCase")]
pub struct Condition {
    #[serde(rename = "match", skip_serializing_if = "Option::is_none")]
    pub cond_match: Option<ConditionMatch>,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub conditions: Vec<Condition>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub chunk: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub chunk_start: Option<u32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub chunk_end: Option<u32>,
}

#[serde(rename_all = "snake_case")]
pub enum ConditionMatch {
    Any,
    All,
}

#[serde(rename_all = "camelCase")]
pub struct Mapping {
    pub mapped_param: String,
    pub ranges: Vec<MappingRange>,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub unmapped_params: Vec<UnmappedParam>,
}

#[serde(rename_all = "camelCase")]
pub struct MappingRange {
    pub start: Option<MappingBound>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub end: Option<MappingBound>,
    pub chunk_start: u32,
    pub chunk_end: u32,
}

pub struct UnmappedParam {
    pub parameter: String,
    pub start: Option<MappingBound>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub end: Option<MappingBound>,
}

#[derive(Copy)]
#[serde(untagged)]
pub enum MappingBound {
    Numeric(f64),
    Bool(bool),
}
}
