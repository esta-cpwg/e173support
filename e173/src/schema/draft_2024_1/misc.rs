use crate::e173_types;

e173_types! {

#[serde(rename_all = "camelCase")]
#[cfg_attr(target_arch = "wasm32", tsify(string_enum))]
#[derive(PartialEq, Eq, Hash)]
pub enum ParameterAccess {
    ReadActual,
    ReadTarget,
    Write,
}

#[serde(rename_all = "camelCase")]
#[cfg_attr(target_arch = "wasm32", tsify(string_enum))]
#[derive(PartialEq, Eq, Hash)]
pub enum Access {
    Read,
    Write
}

#[serde(rename_all = "lowercase")]
#[cfg_attr(target_arch = "wasm32", tsify(string_enum))]
pub enum Lifetime {
    Static,
    Persistent,
    Runtime,
}

}
