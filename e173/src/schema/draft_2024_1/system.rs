use std::collections::HashMap;

use crate::e173_types;

use super::{device::Device, DeviceStaticValues};

e173_types! {

#[serde(rename_all = "camelCase")]
pub struct System {
    pub devices: HashMap<String, Device>,
    #[serde(default, skip_serializing_if = "HashMap::is_empty")]
    pub containers: HashMap<String, Container>,
    #[serde(default, skip_serializing_if = "HashMap::is_empty")]
    pub positions: HashMap<String, Position>,
    #[serde(default, skip_serializing_if = "HashMap::is_empty")]
    pub locations: HashMap<String, Location>,
    #[serde(default, skip_serializing_if = "HashMap::is_empty")]
    pub overlays: HashMap<String, Overlay>,
}

#[serde(rename_all = "camelCase")]
pub struct Container {
    pub devices: Vec<String>,
    #[serde(default, skip_serializing_if = "HashMap::is_empty")]
    pub containers: HashMap<String, Container>,
}

#[serde(rename_all = "camelCase")]
#[cfg_attr(test, derive(Default))]
pub struct Position {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub translate: Option<(f64, f64, f64)>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rotate: Option<(f64, f64, f64)>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub matrix: Option<Vec<f64>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub controller: Option<PositionController>,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub connectors: Vec<Connector>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub model: Option<String>,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub devices: Vec<String>,
    #[serde(default, skip_serializing_if = "HashMap::is_empty")]
    pub positions: HashMap<String, Position>,
}

#[serde(untagged)]
#[cfg_attr(test, derive(Default, PartialEq))]
pub enum Coordinates {
    #[cfg_attr(test, default)]
    Identity,
    TranslateRotate {
    },
    Matrix {
        matrix: Vec<f64>,
    },
}

#[serde(rename_all = "camelCase")]
pub struct PositionController {
    #[serde(skip_serializing_if = "Option::is_none")]
    x_position: Option<PositionDegreeController>,
    #[serde(skip_serializing_if = "Option::is_none")]
    y_position: Option<PositionDegreeController>,
    #[serde(skip_serializing_if = "Option::is_none")]
    z_position: Option<PositionDegreeController>,
    #[serde(skip_serializing_if = "Option::is_none")]
    x_rotate: Option<PositionDegreeController>,
    #[serde(skip_serializing_if = "Option::is_none")]
    y_rotate: Option<PositionDegreeController>,
    #[serde(skip_serializing_if = "Option::is_none")]
    z_rotate: Option<PositionDegreeController>,
    #[serde(skip_serializing_if = "Option::is_none")]
    opacity: Option<PositionDegreeController>,
}

#[serde(rename_all = "camelCase")]
pub struct PositionDegreeController {
    pub device: String,
    pub parameter: String,
}

#[serde(rename_all = "camelCase")]
pub struct Connector {
    start: ConnectorStart,
    end: (f64, f64, f64),
    style: ConnectorStyle,
}

#[serde(untagged)]
pub enum ConnectorStart {
    Device(String),
    Position((f64, f64, f64)),
}

#[serde(rename_all = "kebab-case")]
#[cfg_attr(target_arch = "wasm32", tsify(string_enum))]
pub enum ConnectorStyle {
    ChainMetal,
    ChainSynthetic,
    RopeFiber,
    RopeSteelWire,
    RopeNatural,
    RopeKevlar,
    Webbing,
    Scissor,
    HydraulicCylinder,
    HydraulicBox,
    Other,
}

#[serde(rename_all = "camelCase")]
pub struct Location {
    pub translate: (f64, f64, f64),
    pub name: String,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub focused_devices: Vec<String>,
}

#[serde(rename_all = "camelCase")]
pub struct Overlay {
    #[serde(skip_serializing_if = "Option::is_none")]
    static_values: Option<DeviceStaticValues>,
}

}
