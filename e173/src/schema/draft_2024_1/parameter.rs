use std::collections::HashSet;

use crate::e173_types;

use super::misc::{Lifetime, ParameterAccess};

e173_types! {

#[serde(rename_all = "camelCase")]
pub struct ParameterClass {
    #[serde(rename = "@name")]
    pub name: String,
    #[serde(rename = "@description", skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    pub data_type: DataType,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub unit: Option<Unit>,
}

#[derive(PartialEq)]
#[serde(rename_all = "kebab-case")]
#[cfg_attr(target_arch = "wasm32", tsify(string_enum))]
pub enum DataType {
    Number,
    String,
    Binary,
    Boolean,
    Uuid,
}

#[derive(PartialEq)]
pub struct Unit {
    name: UnitName,
    #[serde(skip_serializing_if = "Option::is_none")]
    exponent: Option<i32>
}

#[derive(PartialEq)]
#[serde(rename_all = "kebab-case")]
#[cfg_attr(target_arch = "wasm32", tsify(string_enum))]
pub enum UnitName {
    Ampere,
    Candela,
    Kelvin,
    Kilogram,
    Meter,
    Second,
    DegreeCelsius,
    Hertz,
    Joule,
    Lumen,
    Lux,
    Newton,
    NewtonMeter,
    Ohm,
    Pascal,
    Radian,
    RadianPerSecond,
    RadianPerSecondSquared,
    Steradian,
    Volt,
    Watt,
    CubicMeter,
    MeterPerSecond,
    MeterPerSecondSquared,
    SquareMeter,
    AmpHours,
    Byte,
    BytePerSecond,
    Degree,
    Ratio,
    LogDb,
    Rpm,
    Other,
    None,
}

#[serde(rename_all = "camelCase")]
pub struct Parameter {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub library: Option<String>,
    pub class: String,
    #[serde(rename = "@friendlyName", skip_serializing_if = "Option::is_none")]
    pub friendly_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dynamic_minimum: Option<u32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dynamic_maximum: Option<u32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub count: Option<u32>,
    pub access: HashSet<ParameterAccess>,
    pub lifetime: Lifetime,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub atomic_identifier: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub minimum: Option<ParameterValue>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub maximum: Option<ParameterValue>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub minimum_modifier: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub maximum_modifier: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub default: Option<ParameterValue>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub wrapping: Option<bool>,
}

#[serde(untagged)]
pub enum ParameterValue {
    Numeric(f64),
    Bool(bool),
}

}
