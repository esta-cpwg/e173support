mod device;
mod esta_dmx;
mod library;
mod localization;
mod misc;
mod parameter;
mod serializer;
mod structure;
mod system;
mod validate;
mod version;

use std::collections::HashMap;

pub use device::*;
pub use esta_dmx::*;
pub use library::*;
pub use localization::*;
pub use misc::*;
pub use parameter::*;
pub use serializer::*;
pub use structure::*;
pub use system::*;
pub use validate::*;
pub use version::*;

use crate::e173_types;

e173_types! {

// TODO: type-state - make this an 'unchecked' type and have validate return a checked document
pub struct E173Document {
    pub e173doc: DocumentBody,
    #[serde(rename = "$schema")]
    pub schema: String,
}

#[serde(rename_all = "camelCase")]
pub struct DocumentBody {
    #[serde(default, skip_serializing_if = "HashMap::is_empty")]
    pub device_classes: HashMap<String, HashMap<String, DeviceClass>>,
    #[serde(default, skip_serializing_if = "HashMap::is_empty")]
    pub libraries: HashMap<String, HashMap<String, Library>>,
    #[serde(default, skip_serializing_if = "HashMap::is_empty")]
    pub systems: HashMap<String, System>
}

}
