use std::collections::HashMap;

use serde::Serialize;

use crate::schema::Parameter;

#[derive(Clone, Debug, Serialize)]
pub struct ParameterDatabase {
    parameters: HashMap<String, Parameter>,
}
