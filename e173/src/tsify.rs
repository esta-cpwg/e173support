#![allow(clippy::wrong_self_convention)]

// Commented items in this module are from the original Tsify 'json' feature, see their README:
// https://github.com/madonoharu/tsify/tree/66cddfe680ae429be0063cf6e6b1b5be34bb7ce2?tab=readme-ov-file#crate-features

// Might need to uncomment after profiling, see discussion of performance tradeoffs here:
// https://rustwasm.github.io/docs/wasm-bindgen/reference/arbitrary-data-with-serde.html

pub use e173_ts::*;
// #[cfg(all(feature = "json", not(feature = "js")))]
// pub use gloo_utils::format::JsValueSerdeExt;
use wasm_bindgen::{JsCast, JsValue};

pub trait Tsify {
    type JsType: JsCast;

    const DECL: &'static str;
    const MAPS_AS_OBJECTS: bool;

    // #[cfg(all(feature = "json", not(feature = "js")))]
    // #[inline]
    // fn into_js(&self) -> serde_json::Result<Self::JsType>
    // where
    //     Self: serde::Serialize,
    // {
    //     JsValue::from_serde(self).map(JsCast::unchecked_from_js)
    // }

    // #[cfg(all(feature = "json", not(feature = "js")))]
    // #[inline]
    // fn from_js<T: Into<JsValue>>(js: T) -> serde_json::Result<Self>
    // where
    //     Self: serde::de::DeserializeOwned,
    // {
    //     js.into().into_serde()
    // }

    #[inline]
    fn into_js(&self) -> Result<Self::JsType, serde_wasm_bindgen::Error>
    where
        Self: serde::Serialize,
    {
        if Self::MAPS_AS_OBJECTS {
            self.serialize(&serde_wasm_bindgen::Serializer::new().serialize_maps_as_objects(true))
        } else {
            serde_wasm_bindgen::to_value(self)
        }
        .map(JsCast::unchecked_from_js)
    }

    #[inline]
    fn from_js<T: Into<JsValue>>(js: T) -> Result<Self, serde_wasm_bindgen::Error>
    where
        Self: serde::de::DeserializeOwned,
    {
        serde_wasm_bindgen::from_value(js.into())
    }
}
