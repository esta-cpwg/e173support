use crate::{
    controller,
    schema::{self, DeviceClass, E173Document},
    Error,
};
use wasm_bindgen::prelude::*;

/// Parses and validates a UDR document.
///
/// This function performs validation beyond what can be done with the UDR JSON schemas. As one of
/// many examples, it validates the mutual exclusion between 'Dynamic Minimum|Maximum Count' and
/// 'Count' in the Parameter object (BSR E1.73-1 sect. 8.2.3).
///
/// @param {string|object} doc - A UDR document, either as a JSON string or a JS object.
/// @returns {E173Document} A validated UDR document.
/// @throws {Error} If validation fails.
#[wasm_bindgen(skip_jsdoc, js_name = importUdr)]
pub fn import_udr(doc: JsValue) -> Result<schema::E173Document, JsValue> {
    let document: schema::E173Document = if doc.is_string() {
        serde_json::from_str(&doc.as_string().unwrap()).map_err(Error::json_parse)?
    } else if doc.is_object() {
        serde_wasm_bindgen::from_value(doc).map_err(|err| Error::parse(&err.to_string()))?
    } else {
        return Err((Error::InvalidArgument).into());
    };

    schema::validate_doc(&document)?;
    Ok(document)
}

/// Validates a UDR document represented in JavaScript.
///
/// This function performs validation beyond what can be done with the UDR JSON schemas. As one of
/// many examples, it validates the mutual exclusion between 'Dynamic Minimum|Maximum Count' and
/// 'Count' in the Parameter object (BSR E1.73-1 sect. 8.2.3).
///
/// @param {E173Document} document - A UDR document.
/// @throws {Error} If validation fails.
#[wasm_bindgen(skip_jsdoc, js_name = validateUdr)]
pub fn validate_udr(document: E173Document) -> Result<(), JsValue> {
    Ok(schema::validate_doc(&document)?)
}

/// Validates a UDR Device Class represented in JavaScript.
///
/// This function performs validation beyond what can be done with the UDR JSON schemas. As one of
/// many examples, it validates the mutual exclusion between 'Dynamic Minimum|Maximum Count' and
/// 'Count' in the Parameter object (BSR E1.73-1 sect. 8.2.3).
///
/// @param {DeviceClass} device_class - A UDR device class.
/// @throws {Error} If validation fails.
#[wasm_bindgen(skip_jsdoc, js_name = validateUdrDeviceClass)]
pub fn validate_udr_device_class(device_class: DeviceClass) -> Result<(), JsValue> {
    Ok(schema::validate_device_class(&device_class)?)
}

/// Creates a controller parameter database from a UDR Device Class
///
/// The parameter database contains the data from the UDR Device Class, transformed into a format
/// that is more useful for controller software.
///
/// @param {DeviceClass} device_class - The device class to transform.
/// @returns {ParameterDatabase} Parameter database generated from the device class.
/// @throws {Error} If validation fails on the device class. Usually this means there is something
///                 invalid in the ESTA DMX serializer.
#[wasm_bindgen(skip_jsdoc, js_name = createParameterDatabase)]
pub fn create_parameter_database(
    device_class: schema::DeviceClass,
) -> Result<controller::ParameterDatabase, Error> {
    controller::create_parameter_database(&device_class)
}
