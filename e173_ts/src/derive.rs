use proc_macro2::TokenStream;
use quote::quote;
use syn::{parse_quote, DeriveInput};

use crate::{container::Container, parser::Parser, wasm_bindgen};

pub fn expand(input: DeriveInput) -> syn::Result<TokenStream> {
    let cont = Container::from_derive_input(&input)?;

    let parser = Parser::new(&cont);
    let decl = parser.parse();

    let tokens = wasm_bindgen::expand(&cont, decl);

    cont.check()?;

    Ok(tokens)
}

pub fn expand_by_attr(args: TokenStream, input: DeriveInput) -> syn::Result<TokenStream> {
    let mut cloned_input = input.clone();
    let attr: syn::Attribute = parse_quote!(#[tsify(#args)]);
    cloned_input.attrs.push(attr);

    let derived = expand(cloned_input)?;

    let tokens = quote! {
      #input
      #derived
    };

    Ok(tokens)
}
