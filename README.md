# E1.73 Support Library

A library implementing various aspects of the ESTA BSR E1.73 standard, and building on it with functionality that is useful to various classes of implementers.

## Use in WASM

This library can be used as a standard Rust library, but it is also designed to be compiled to WebAssembly for use in web projects. We compile and publish as an NPM package using the [`wasm-pack`](https://rustwasm.github.io/docs/wasm-pack/) tool.

## Building for WASM

You need `wasm-pack` to build for WASM (see above).

We currently have a custom build script `build_wasm.sh` in the root of the repo to build the WASM package. This is for two reasons. First, we require a postprocessing step to build the library for WASM, because of our desire to use TypeScript string enums and [`wasm-bindgen`'s limitations with them](https://github.com/rustwasm/wasm-bindgen/issues/3057). Second, `wasm-pack` gets tripped up by the fact that there are multiple crates in this repo, and doesn't copy the right README file to the package.

If you are running on Windows, this script should run in Cygwin or MSYS2, or you can trivially reproduce it as a powershell script or similar.

If you want to develop and test the WASM functionality of the library in a Node environment (e.g. running automated tests for your web project), you currently need a version of wasm-pack newer than the latest release, because the solution for [this issue](https://github.com/rustwasm/wasm-pack/issues/1039), while merged, has not yet made it into a release.

So for example, to build from source, you can do:

```
$ cargo install wasm-pack --git https://github.com/rustwasm/wasm-pack.git
```

## Acknowledgments

- Thanks to Jonathan Kemble for designing the format of the ESTA DMX serializer and the algorithms by which it is translated into a database that is useful for controllers, and for providing the original C++ implementation from which the code in the `controller` module was largely adapted.
- Thanks to [@MadonoHaru](https://github.com/madonoharu) for the [Tsify](https://github.com/madonoharu/tsify) crate, which is integrated into this library as the basis of `e173_ts`, and helps immensely with generating and type-checking the TypeScript/JavaScript FFI.
